package com.minhash;

import com.google.common.collect.Sets;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MinHash {
    private static final int SHINGLE_LEN = 2;

    private final List<Function<String, Integer>> functions;

    public MinHash(int numFunctions) {
        Random r = new Random();
        functions = new ArrayList<>();

        for (int i = 0; i < numFunctions; i++) {
            int a = r.nextInt(100);
            int b = r.nextInt(100);
            Function<String, Integer> function = x -> Math.abs((a * x.hashCode() >> 4 + b) % Integer.MAX_VALUE);
            functions.add(function);
        }
    }


    public Set getShingle(String strNew) {
        Set set = new HashSet();
        String str = strNew.toLowerCase();
        String words[] = str.split(" ");
        int shinglesNumber = words.length - SHINGLE_LEN;

        for (int i = 0; i <= shinglesNumber; i++) {
            String shingle = "";

            for (int j = 0; j < SHINGLE_LEN; j++) {
                shingle = shingle + words[i+j] + " ";
            }
            set.add(shingle);

        }
        return set;
    }

    public Set<Integer> minHashForSet(Set<String> shingles) {
        return functions.stream()
                .map(function -> shingles.stream()
                        .map(function)
                        .min(Integer::compare)
                        .get())
                .collect(Collectors.toSet());
    }

    public double computeSimilarity(Set<Integer> s1, Set<Integer> s2) {
        Set<Integer> union = Sets.union(s1, s2);
        Set<Integer> intersection = Sets.intersection(s1, s2);
        return (1.0 * intersection.size()) / union.size();
    }
}
