package com.minhash;

import java.util.Set;

public class Main {

    public static void main(String[] args) {

        MinHash minHash = new MinHash(20);

        Set set1 = minHash.getShingle("The MinHash scheme may be seen as an instance of locality sensitive hashing," +
                " a collection of techniques for using hash functions to map large sets of objects down to smaller hash values in such a ");

        Set set2 = minHash.getShingle("for using hash functions to map large sets of objects down to smaller hash values" +
                " in such a The MinHash scheme may be seen as an instance of locality sensitive hashing, a collection of techniques");


        Set<Integer> setHash1 = minHash.minHashForSet(set1);
        Set<Integer> setHash2 = minHash.minHashForSet(set2);


        System.out.println("Similarity for sets is : " + minHash.computeSimilarity(setHash1, setHash2));
    }
}
